#include <stdlib.h>
#include "read_write.h"
#include "read_write_messages.h"

int main(int argc, char **argv) {
    if (argc != 2) {
        fprintf(stderr, "Wrong arguments, please specify the path to the bmp file after the program name\n");
        return 0;
    }
    struct image image = {0};
    enum read_status status_open = open_bmp(argv[1], &image);
    if (status_open != READ_OK) {
        fprintf(stderr, "The program closed with the error: %s\n", get_open_message(status_open));
        return 0;
    }
    struct image result = rotate90(&image);
    free(image.data);
    enum write_status status_close = save_bmp(argv[1], &result);
    free(result.data);
    if (status_close == WRITE_OK)
        fprintf(stderr, "%s\n", get_close_message(status_close));
    else
        fprintf(stderr, "The program closed with the error: %s\n", get_close_message(status_close));
    return 0;
}
