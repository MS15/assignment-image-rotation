#include <stdio.h>
#include <stdlib.h>
#include "image_change.h"
#include "read_write.h"

const uint32_t MB = 0x4d42;

struct bmp_header create_header(struct image const *image, const int8_t padding) {
    uint32_t biSizeImage = image->height * (image->width * sizeof(struct pixel) + padding);
    return (struct bmp_header) {.bfType = MB,
            .bfileSize = sizeof(struct bmp_header) + biSizeImage,
            .bfReserved = 0,
            .bOffBits = 54,
            .biSize = 40,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = biSizeImage,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0};
}

int8_t count_padding(struct image const *image) {
    return (4 - (image->width * 3 % 4)) % 4;
}

enum read_status from_bmp(FILE *file_in, struct image *image) {
    struct bmp_header header = {0};
    fread(&header, sizeof(struct bmp_header), 1, file_in);
    if(header.bfType != MB) {
        fclose(file_in);
        return READ_INVALID_SIGNATURE;
    }
    image->width = header.biWidth;
    image->height = header.biHeight;
    struct pixel *data = malloc(image->width * image->height * sizeof(struct pixel));
    const int8_t padding = count_padding(image);
    for (size_t i = 0; i < image->height; i++) {
        fread(&(data[i * image->width]), sizeof(struct pixel), image->width, file_in);
        fseek(file_in, padding, SEEK_CUR);
    }
    image->data = data;
    return READ_OK;
}

enum read_status open_bmp(char* filename, struct image *image){
    FILE *file_in = fopen(filename, "rb");
    if(!file_in)
        return OPEN_ERROR;
    enum read_status status = from_bmp(file_in, image);
    if(fclose(file_in) != 0)
        return CLOSE_ERROR;
    return status;
}

enum write_status to_bmp(FILE *file_out, struct image const *image) {
    const int8_t padding = count_padding(image);
    struct bmp_header header = create_header(image, padding);
    fwrite(&header, sizeof(struct bmp_header), 1, file_out);
    const int8_t rubbish = 0;
    for (size_t i = 0; i < image->height; i++) {
        fwrite(&image->data[i * image->width], sizeof(struct pixel), image->width, file_out);
        fwrite(&rubbish, 1, padding, file_out);
    }
    return WRITE_OK;
}

enum write_status save_bmp(char* filename, struct image const *image){
    FILE *file_out = fopen(filename, "wb");
    if(!file_out)
        return WRITE_ERROR;
    enum write_status status = to_bmp(file_out, image);
    if(status != WRITE_OK)
        return status;
    if(fclose(file_out) != 0)
        return WRITE_CLOSE_ERROR;
    return WRITE_OK;
}

