#ifndef _BMPROTATE_READ_WRITE_H_
#define _BMPROTATE_READ_WRITE_H_

#include <stdio.h>
#include <stdint.h>
#include "image_change.h"

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

enum read_status  {
    READ_OK,
    READ_INVALID_SIGNATURE,
    OPEN_ERROR,
    CLOSE_ERROR
    /* коды других ошибок  */
};

enum read_status open_bmp(char* filename, struct image *image);

/*  serializer   */
enum write_status  {
    WRITE_OK,
    WRITE_ERROR,
    WRITE_CLOSE_ERROR
    /* коды других ошибок  */
};

enum write_status save_bmp(char* filename, struct image const *image);

#endif
