#include "read_write_messages.h"

const char *open_message[] = {
        [READ_INVALID_SIGNATURE] = "The file has an invalid signature",
        [OPEN_ERROR] = "Couldn't open a file with this name",
        [CLOSE_ERROR] = "An error occurred while closing the file"
};

const char *close_message[] = {
        [WRITE_ERROR] = "Сould not open file to write data",
        [WRITE_CLOSE_ERROR] = "Failed to complete data recording",
        [WRITE_OK] = "The picture was successfully rotated"
};

const char *get_open_message(enum read_status status) {
    return open_message[status];
}

const char *get_close_message(enum write_status status) {
    return close_message[status];
}