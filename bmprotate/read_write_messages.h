#ifndef _BMPROTATE_READ_WRITE_MESSAGES_H_
#define _BMPROTATE_READ_WRITE_MESSAGES_H_

#include "read_write.h"

const char *get_open_message(enum read_status status);

const char *get_close_message(enum write_status status);

#endif
