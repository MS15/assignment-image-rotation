#include "image_change.h"
#include <stdlib.h>

struct image rotate90(struct image const *image) {
    struct image result_image = {.width = image->height,.height = image->width,0};
    struct pixel *data = malloc(image->width * image->height * sizeof(struct pixel));
    for (size_t i = 0; i < image->height; i++)
        for (size_t j = 0; j < image->width; j++)
            data[j * image->height + (image->height - 1 - i)] = image->data[i * image->width + j];
    result_image.data = data;
    return result_image;
}
