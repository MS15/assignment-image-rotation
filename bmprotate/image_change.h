#ifndef _BMPROTATE_IMAGE_CHANGE_H_
#define _BMPROTATE_IMAGE_CHANGE_H_

#include <stdint.h>

struct pixel {
    uint8_t b, g, r;
};
struct image {
    uint32_t width, height;
    struct pixel *data;
};

struct image rotate90(struct image const *image);

#endif
